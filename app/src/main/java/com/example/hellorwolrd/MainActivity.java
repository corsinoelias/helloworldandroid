package com.example.hellorwolrd;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String hello = getString(R.string.hello);
        String nombreApp = getString(R.string.app_name);


        //Codi per mostrar el dia de la setmana que estem en un Toast
        String[] llistaPlanetes = getResources().getStringArray(R.array.dias);
        int dia = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        Toast.makeText(this, "Hoy es " + llistaPlanetes[dia - 2], Toast.LENGTH_LONG).show();

        Toast.makeText(this, "Mensaje: " + hello + "Mi app:" + nombreApp, Toast.LENGTH_SHORT).show();
        //getString(R.string.xxxx), getResources
        // findViewById

        TextView tvSaludo = (TextView) findViewById(R.id.tvSaludo);
        tvSaludo.setText("Adios mundo Cruel");
        Toast.makeText(this, tvSaludo.getText(), Toast.LENGTH_SHORT).show();
    }
}
